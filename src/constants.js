const INTENTS = 771;
const HOUR_IN_MS = 1000 * 60 * 60

module.exports = {INTENTS, HOUR_IN_MS}
