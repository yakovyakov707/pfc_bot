const config = require('./discordBotConfig.json')
const { HOUR_IN_MS } = require('./constants');
/* eslint-disable */
const regexp = new RegExp("(?<=^|[^а-я])(([уyu]|[нзnz3][аa]|(хитро|не)?[вvwb][зz3]?[ыьъi]|[сsc][ьъ']|(и|[рpr][аa4])[зсzs]ъ?|([оo0][тбtb6]|[пp][оo0][дd9])[ьъ']?|(.\B)+?[оаеиeo])?-?([еёe][бb6](?!о[рй])|и[пб][ае][тц]).*?|([нn][иеаaie]|([дпdp]|[вv][еe3][рpr][тt])[оo0]|[рpr][аa][зсzc3]|[з3z]?[аa]|с(ме)?|[оo0]([тt]|дно)?|апч)?-?[хxh][уuy]([яйиеёюuie]|ли(?!ган)).*?|([вvw][зы3z]|(н|[сc][уuy][кk])[аa])?-?[бb6][лl]([яy](?!(х|ш[кн]|мб)[ауеыио]).*?|[еэe][дтdt][ь']?)|([рp][аa][сзc3z]|[знzn][аa]|[соsc]|[вv][ыi]?|[пp]([еe][рpr][еe]|[рrp][оиioеe]|[оo0][дd])|и[зс]ъ?|[аоao][тt])?[пpn][иеёieu][зz3][дd9].*?|([зz3][аa])?[пp][иеieu][дd][аоеaoe]?[рrp](ну.*?|[оаoa][мm]|([аa][сcs])?([иiu]([лl][иiu])?[нщктлtlsn]ь?)?|([оo](ч[еиei])?|[аa][сcs])?[кk]([оo]й)?|[юu][гg])[ауеыauyei]?|[мm][аa][нnh][дd]([ауеыayueiи]([лl]([иi][сзc3щ])?[ауеыauyei])?|[оo][йi]|[аоao][вvwb][оo](ш|sh)[ь']?([e]?[кk][ауеayue])?|юк(ов|[ауи])?)|[мm][уuy][дd6]([яyаиоaiuo0].*?|[еe]?[нhn]([ьюия'uiya]|ей))|мля([тд]ь)?|лять|([нз]а|по)х|м[ао]л[ао]фь([яию]|[её]й))(?=($|[^а-я]))")
const AMOUNT_OF_WORDS_FOR_MUTE = 5

const writeToAuthorMessage = (author, message) => {
	author.send(message)
}

const muteAuthorForAnHour = (message, author) => {
	const member =  message.guild.members.cache.get(author.id)
	member.timeout(HOUR_IN_MS, "Bad words use")
	writeToAuthorMessage(author, `Вы были получили мут на сервере Passion for coding, причина: использование ненормативной лексики`)
}

module.exports = {
	verifyNickname: function(m){
		if(regexp.test(m.displayName)){
			m.guild.channels.cache.get(config.log_channel).send(`warning: user <@${m.id}> has inappropriate nickname`)
		}
	},
	handleSendMessage: function (message, cache) {
		const {content, author} = message
		const authorId = author.id
		const authorSignatureFromCache = cache[authorId]
		const incrementedBadWordsAmount =  authorSignatureFromCache?.badWordsAmount? authorSignatureFromCache.badWordsAmount + 1 : 1
		if( regexp.test(content) ) {
			cache[authorId] = { ...cache[authorId], badWordsAmount:incrementedBadWordsAmount }
			if(!cache[authorId].createdAt) cache[authorId].createdAt = new Date()
		}

		if(cache[authorId]?.badWordsAmount === AMOUNT_OF_WORDS_FOR_MUTE){
			muteAuthorForAnHour(message, author)
			delete cache[authorId]
		}
	}
}
