const { Client } = require('discord.js');
const config = require('./discordBotConfig.json')
const { INTENTS, HOUR_IN_MS } = require('./constants');
const { verifyNickname, handleSendMessage } = require('./regexp.js')

const cache = Object.create(null)

setInterval( () => {
	for(const [key, value] of Object.entries(cache)){
		const {createdAt} = value
		const currentTimeInMS = Date.now()
		const createdAtInMs = new Date(createdAt).getTime()

		if(currentTimeInMS - createdAtInMs > HOUR_IN_MS){
			delete cache[key]
		}
	}
} ,1000)

const bot = new Client({
	intents: INTENTS
});

bot.on('guildMemberAdd', m => {
	verifyNickname(m)
	m.roles.add(config.autorole,'autorole')
})
bot.on('guildMemberUpdate', (old,m) => {
	verifyNickname(m)
})

bot.on('messageCreate', message => handleSendMessage(message, cache))
bot.on('messageUpdate', (oldMessage,newMessage) => handleSendMessage(newMessage, cache))

bot.login(config.token)
